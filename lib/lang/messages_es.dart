// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alert" : MessageLookupByLibrary.simpleMessage("Alerta"),
    "button" : MessageLookupByLibrary.simpleMessage("¿Que Departamento?"),
    "dep0" : MessageLookupByLibrary.simpleMessage("Seleccionar Departamento"),
    "dep1" : MessageLookupByLibrary.simpleMessage("Departamento de Ventas"),
    "dep2" : MessageLookupByLibrary.simpleMessage("Departamento de Recursos Humanos"),
    "dep3" : MessageLookupByLibrary.simpleMessage("Departamento de Inventario"),
    "hint" : MessageLookupByLibrary.simpleMessage("ej. Fluffy"),
    "label" : MessageLookupByLibrary.simpleMessage("Nombre de mascota"),
    "name" : MessageLookupByLibrary.simpleMessage("El nombre de tu mascota es"),
    "pet" : MessageLookupByLibrary.simpleMessage("Ingrese el nombre de su mascota"),
    "print1" : MessageLookupByLibrary.simpleMessage("Ventas seleccionadas"),
    "print2" : MessageLookupByLibrary.simpleMessage("Recursos Humanos seleccionados"),
    "print3" : MessageLookupByLibrary.simpleMessage("Inventario seleccionado"),
    "title" : MessageLookupByLibrary.simpleMessage("Bienvienido a mi aplicación")
  };
}
