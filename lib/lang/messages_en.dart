// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "alert" : MessageLookupByLibrary.simpleMessage("Show Alert"),
    "button" : MessageLookupByLibrary.simpleMessage("Which Department?"),
    "dep0" : MessageLookupByLibrary.simpleMessage("Select Department"),
    "dep1" : MessageLookupByLibrary.simpleMessage("Sales Department"),
    "dep2" : MessageLookupByLibrary.simpleMessage("Human Resources Department"),
    "dep3" : MessageLookupByLibrary.simpleMessage("Inventory Department"),
    "hint" : MessageLookupByLibrary.simpleMessage("ex. Fluffy"),
    "label" : MessageLookupByLibrary.simpleMessage("Pet name"),
    "name" : MessageLookupByLibrary.simpleMessage("The name of your pet is"),
    "pet" : MessageLookupByLibrary.simpleMessage("Enter the name of your pet"),
    "print1" : MessageLookupByLibrary.simpleMessage("Sales selected"),
    "print2" : MessageLookupByLibrary.simpleMessage("Human Resources selected"),
    "print3" : MessageLookupByLibrary.simpleMessage("Inventory selected"),
    "title" : MessageLookupByLibrary.simpleMessage("Welcome to my app")
  };
}
