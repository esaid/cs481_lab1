import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'lang/messages_all.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

//Created by  Esai Delgado, Zsuzsanna Dianovics, Tyler Young

void main() {
  runApp(MyApp());
}

enum Department { sales, human_resources, inventory }
String NameOfPet ='';

class MyApp extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        const MyAppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('es', 'MX'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Internationalization/Dialog App',
      home: ButtonTest(),
    );
  }
}

class ButtonTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    //Code from flutter.dev
  Future<void> _askedToLead() async {
    switch (await showDialog<Department>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(title: Text(MyAppLocalizations.of(context).dep0),
            children: <Widget>[
              SimpleDialogOption(onPressed: () {
                  Navigator.pop(context, Department.sales);},
                child: Text(MyAppLocalizations.of(context).dep1),
              ),
              SimpleDialogOption(onPressed: () {
                  Navigator.pop(context, Department.human_resources);},
                child: Text(MyAppLocalizations.of(context).dep2),
              ),
              SimpleDialogOption(onPressed: () {
                  Navigator.pop(context, Department.inventory);},
                child: Text(MyAppLocalizations.of(context).dep3),
              ),
            ],
          );
        })) {
      case Department.sales:
        print(MyAppLocalizations.of(context).print1);
        break;
      case Department.human_resources:
        print(MyAppLocalizations.of(context).print2);
        break;
      case Department.inventory:
        print(MyAppLocalizations.of(context).print3);
        break;
    }
}
//Dialog returns name of pet
ShowResultDialog(BuildContext context)
{
  return showDialog(
    context: context,
    barrierDismissible: true,
      builder: (BuildContext context){
      return AlertDialog(
        title: Text(MyAppLocalizations.of(context).name + ' $NameOfPet'),
        actions: [
          FlatButton(
            child: Text(MyAppLocalizations.of(context).confirm),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
      }
  );
}

//more dialog
showAlertDialog(BuildContext context)
{
  //Saves the user input
  return showDialog(
  context: context,
    barrierDismissible: true, //Allows the user to dismiss the dialog by pressing outside the popup
    builder: (BuildContext context)
    {
      return AlertDialog(
        title: Text(MyAppLocalizations.of(context).pet),
        content: new Row(
          children: [
            new Expanded( //Children within expanded expand inside of empty space
              child: new TextField(
                autofocus: true,
                decoration: new InputDecoration(
                  labelText: MyAppLocalizations.of(context).label, hintText: MyAppLocalizations.of(context).hint
                ),
                onChanged: (value){
                  NameOfPet= value;
                },
              ),
            )
          ],
        ),
        actions: [
          FlatButton(
            child: Text(MyAppLocalizations.of(context).confirm),
            onPressed: (){
              ShowResultDialog(context);
              }
          )
        ],
      );
    }
  );
}



  //Creating buttons
    return Scaffold(
      appBar: AppBar(backgroundColor: Colors.purple,
        title: Text(MyAppLocalizations.of(context).title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(color: Colors.purple[300], textColor: Colors.white,
              child: Text(MyAppLocalizations.of(context).alert),
              onPressed: () {
              showAlertDialog(context);}
            ),

            RaisedButton(color: Colors.purple[300], textColor: Colors.white, 
          child: Text(MyAppLocalizations.of(context).button), 
          onPressed: () {_askedToLead();},
            ),
          ],
        )
        )
    );
  }
}

//From intl dart package
class MyAppLocalizations {
  MyAppLocalizations(this.localeName);
  
  final String localeName;

  static Future<MyAppLocalizations> load(Locale locale) {
    final String lang = locale.countryCode.isEmpty ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(lang);
    return initializeMessages(localeName).then(
      (_) {
        return MyAppLocalizations(localeName);
      }
    );
  }

  static MyAppLocalizations of(BuildContext context) {
    return Localizations.of<MyAppLocalizations>(context, MyAppLocalizations);
}
  //Used for calling translated text
  String get title {
    return Intl.message('Welcome to my app', name: 'title', desc: 'example text', locale: localeName);
  }
  String get button {
    return Intl.message('Which Department?', name: 'button', desc: 'text for button', locale: localeName);
  }
  String get dep0 {
    return Intl.message('Select Department', name: 'dep0', desc: 'inital select', locale: localeName);
  }
  //options
  String get dep1 {
    return Intl.message('Sales Department', name: 'dep1', desc: 'first option', locale: localeName);
  }
  String get dep2 {
    return Intl.message('Human Resources Department', name: 'dep2', desc: 'second option', locale: localeName);}
  String get dep3 {
    return Intl.message('Inventory Department', name: 'dep3', desc: 'third option', locale: localeName);}
    //dialog
  String get alert {
    return Intl.message('Show Alert', name: 'alert', desc: 'opens up dialog', locale: localeName);}
  String get pet {
    return Intl.message('Enter the name of your pet', name: 'pet', desc: 'type in pet name', locale: localeName);}
  String get label {
    return Intl.message('Pet name', name: 'label', desc: 'goes above text box', locale: localeName);}
  String get hint {
    return Intl.message('ex. Fluffy', name: 'hint', desc: 'inside the text box', locale: localeName);}
  String get confirm {
    return Intl.message('Ok', name: 'confirm', desc: 'confirm pet name', locale: localeName);}
  String get name {
    return Intl.message('The name of your pet is', name: 'name', desc: 'type in pet name', locale: localeName);}
  String get print1 {
    return Intl.message('Sales selected', name: 'print1', desc: 'print in console', locale: localeName);}
  String get print2 {
    return Intl.message('Human Resources selected', name: 'print2', desc: 'print in console', locale: localeName);}
  String get print3 {
    return Intl.message('Inventory selected', name: 'print3', desc: 'print in console', locale: localeName);}
}
//From flutter.dev i18n page
class MyAppLocalizationsDelegate extends LocalizationsDelegate<MyAppLocalizations> {
  const MyAppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);
  @override
  Future<MyAppLocalizations> load(Locale locale) => MyAppLocalizations.load(locale);
  @override
  bool shouldReload(MyAppLocalizationsDelegate prev) => false;
}

